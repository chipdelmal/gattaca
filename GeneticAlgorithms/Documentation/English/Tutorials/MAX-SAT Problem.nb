(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     24697,        751]
NotebookOptionsPosition[     21445,        632]
NotebookOutlinePosition[     21878,        649]
CellTagsIndexPosition[     21835,        646]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Tutorial", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/tutorial/MAX-SAT Problem", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->1427428552],

Cell["XXXX", "Keywords",
 CellID->1251852827]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->218895918],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->795871300],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->199739161],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->40625308],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->357121918],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->35949532],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->929432370],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["MAX-SAT Problem", "Title",
 CellID->509267359],

Cell["\<\
The maximum satisfiability problem is the following one: given a logic \
formula in disjunctive normal form find the combination of inputs that yields \
the maximum number of true clauses.\
\>", "Text",
 CellChangeTimes->{{3.559595472937002*^9, 3.559595551733927*^9}, {
  3.55959599703272*^9, 3.5595960024840727`*^9}},
 CellID->1534169418],

Cell[BoxData[GridBox[{
   {"XXXX", Cell["XXXX", "TableText"]},
   {"XXXX", Cell["XXXX", "TableText"]},
   {"XXXX", Cell["XXXX", "TableText"]}
  }]], "DefinitionBox",
 CellID->2096742444],

Cell["XXXX.", "Caption",
 CellID->1891092685],

Cell[CellGroupData[{

Cell[TextData[{
 "The first thing we need to do to solve our problem is to protect the value \
of the variable \"x\" that we need to be \"free\" so that we can create and \
evaluate our logic formulas. After that we define a function that generates \
random DNF and we also need a pair of functions that allow us to convert our \
populations from binary (0,1) to logical (False, True); something that is \
needed as the package works only with binary values although ",
 StyleBox["Mathematica",
  FontSlant->"Italic"],
 " evaluates a logic function with logic values and two helper functions that \
will help us evaluate our fitness function."
}], "MathCaption",
 CellChangeTimes->{{3.5595956640328197`*^9, 3.559595719233141*^9}, {
  3.5595959103093224`*^9, 3.559595912637446*^9}, {3.559595973256937*^9, 
  3.559595992399296*^9}, {3.559596666711583*^9, 3.559596740063095*^9}, {
  3.5595971306891537`*^9, 3.55959714681592*^9}},
 CellID->836781195],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Protect", "[", "x", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"GenerateRandomDNFList", "[", 
   RowBox[{
   "clauseMaxSize_", ",", "numberOfVariables_", ",", "numberOfClauses_", ",", 
    "x_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "listOfVariables", ",", "clauseSize", ",", "index", ",", "negative", ",",
       "dnfList"}], "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"dnfList", "=", "\[IndentingNewLine]", 
      RowBox[{"Table", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{
         RowBox[{"clauseSize", "=", 
          RowBox[{"RandomInteger", "[", 
           RowBox[{"{", 
            RowBox[{"1", ",", "clauseMaxSize"}], "}"}], "]"}]}], ";", 
         "\[IndentingNewLine]", 
         RowBox[{"Table", "[", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{
            RowBox[{"index", "=", 
             RowBox[{"RandomInteger", "[", 
              RowBox[{"{", 
               RowBox[{"1", ",", "numberOfVariables"}], "}"}], "]"}]}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"negative", "=", 
             RowBox[{"RandomInteger", "[", 
              RowBox[{"{", 
               RowBox[{"0", ",", "1"}], "}"}], "]"}]}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"If", "[", 
             RowBox[{
              RowBox[{"negative", "\[Equal]", "0"}], ",", 
              RowBox[{"x", "[", "index", "]"}], ",", 
              RowBox[{"!", 
               RowBox[{"x", "[", "index", "]"}]}]}], "]"}]}], 
           "\[IndentingNewLine]", ",", 
           RowBox[{"{", 
            RowBox[{"i", ",", "1", ",", "clauseSize"}], "}"}]}], "]"}]}], 
        "\[IndentingNewLine]", ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", "numberOfClauses"}], "}"}]}], "]"}]}], 
     ";", "\[IndentingNewLine]", "dnfList"}]}], "\[IndentingNewLine]", 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ConvertPopulationToLogic", "[", "population_", "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Table", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Table", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{
           RowBox[{
            RowBox[{"population", "[", 
             RowBox[{"[", "i", "]"}], "]"}], "[", 
            RowBox[{"[", "j", "]"}], "]"}], "\[Equal]", "0"}], ",", "False", 
          ",", "True"}], "]"}], "\[IndentingNewLine]", ",", 
        RowBox[{"{", 
         RowBox[{"j", ",", "1", ",", 
          RowBox[{"Length", "[", 
           RowBox[{"population", "[", 
            RowBox[{"[", "i", "]"}], "]"}], "]"}]}], "}"}]}], "]"}], 
      "\[IndentingNewLine]", ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", "population", "]"}]}], "}"}]}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ConvertPopulationToBinary", "[", "population_", "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Table", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Table", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{
           RowBox[{
            RowBox[{"population", "[", 
             RowBox[{"[", "i", "]"}], "]"}], "[", 
            RowBox[{"[", "j", "]"}], "]"}], "\[Equal]", "False"}], ",", "0", 
          ",", "1"}], "]"}], "\[IndentingNewLine]", ",", 
        RowBox[{"{", 
         RowBox[{"j", ",", "1", ",", 
          RowBox[{"Length", "[", 
           RowBox[{"population", "[", 
            RowBox[{"[", "i", "]"}], "]"}], "]"}]}], "}"}]}], "]"}], 
      "\[IndentingNewLine]", ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", "population", "]"}]}], "}"}]}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"CountTrues", "[", "boolList_", "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "counter", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Total", "[", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"boolList", "[", 
           RowBox[{"[", "i", "]"}], "]"}], "\[Equal]", "True"}], ",", "1", 
         ",", "0"}], "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "1", ",", 
         RowBox[{"Length", "[", "boolList", "]"}]}], "}"}]}], "]"}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EvaluateDisjunctionClause", "[", 
   RowBox[{"list_", ",", "chromosome_", ",", "x_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "assignations", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"assignations", "=", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"x", "[", "i", "]"}], "\[Rule]", 
         RowBox[{"chromosome", "[", 
          RowBox[{"[", "i", "]"}], "]"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", 
          RowBox[{"Length", "[", "chromosome", "]"}]}], "}"}]}], "]"}]}], ";",
      "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Apply", "[", 
       RowBox[{"Or", ",", "list"}], "]"}], "/.", "assignations"}]}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EvaluateConjunctionsList", "[", 
   RowBox[{"list_", ",", "chromosome_", ",", "x_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"trueClauses", ",", "boolList"}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"trueClauses", "=", "0"}], ";", "\[IndentingNewLine]", 
     RowBox[{"boolList", "=", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"EvaluateDisjunctionClause", "[", 
         RowBox[{
          RowBox[{"list", "[", 
           RowBox[{"[", "i", "]"}], "]"}], ",", "chromosome", ",", "x"}], 
         "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", 
          RowBox[{"Length", "[", "list", "]"}]}], "}"}]}], "]"}]}]}]}], 
   "\[IndentingNewLine]", "]"}]}]}], "Input",
 CellChangeTimes->{{3.559595657702097*^9, 3.5595956616338463`*^9}, {
  3.5595957758227367`*^9, 3.559595776104187*^9}, {3.559595839307045*^9, 
  3.559595839525174*^9}, {3.559595982925737*^9, 3.5595959832282753`*^9}, {
  3.559596341913002*^9, 3.559596510672901*^9}, {3.559596573173386*^9, 
  3.559596600772828*^9}, {3.559597117573577*^9, 3.559597117800015*^9}, {
  3.559597173027274*^9, 3.559597183123701*^9}, {3.559597293027521*^9, 
  3.559597295961604*^9}},
 CellLabel->"In[2]:=",
 CellID->2058623809]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Now we are ready to generate the function we want to work on:\
\>", "MathCaption",
 CellChangeTimes->{{3.5595956640328197`*^9, 3.559595719233141*^9}, {
  3.5595959103093224`*^9, 3.559595912637446*^9}, {3.559595973256937*^9, 
  3.559595992399296*^9}, {3.559596119252861*^9, 3.559596138242176*^9}, {
  3.559596193131322*^9, 3.55959619373947*^9}},
 CellID->677454377],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"numberOfVariables", "=", "6"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"functionToAnalyze", "=", 
  RowBox[{"GenerateRandomDNFList", "[", 
   RowBox[{"3", ",", "numberOfVariables", ",", "10", ",", "x"}], 
   "]"}]}]}], "Input",
 CellChangeTimes->{{3.5595957821491957`*^9, 3.559595795244979*^9}, {
  3.5595958427582912`*^9, 3.559595850822661*^9}, {3.559595902495927*^9, 
  3.559595903363749*^9}, {3.559596149677999*^9, 3.5595961764854937`*^9}, {
  3.559596344064702*^9, 3.559596344416791*^9}, {3.559597228395894*^9, 
  3.5595972346522512`*^9}, {3.55959781364755*^9, 3.559597827009121*^9}},
 CellLabel->"In[11]:=",
 CellID->1679203177],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "1", "]"}]}], ",", 
     RowBox[{"!", 
      RowBox[{"x", "[", "2", "]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "2", "]"}]}], ",", 
     RowBox[{"!", 
      RowBox[{"x", "[", "3", "]"}]}], ",", 
     RowBox[{"x", "[", "3", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "2", "]"}]}], ",", 
     RowBox[{"x", "[", "5", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"!", 
     RowBox[{"x", "[", "4", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"x", "[", "2", "]"}], ",", 
     RowBox[{"x", "[", "1", "]"}], ",", 
     RowBox[{"x", "[", "4", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "[", "4", "]"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "5", "]"}]}], ",", 
     RowBox[{"!", 
      RowBox[{"x", "[", "5", "]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"!", 
     RowBox[{"x", "[", "3", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"!", 
     RowBox[{"x", "[", "5", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "1", "]"}]}], ",", 
     RowBox[{"!", 
      RowBox[{"x", "[", "4", "]"}]}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.559595795760179*^9, {3.5595958512225018`*^9, 3.559595856036929*^9}, 
   3.559595904070217*^9, 3.559596010060669*^9, {3.559596150257222*^9, 
   3.559596176730823*^9}, 3.55959634807269*^9, 3.559597250965644*^9, 
   3.559597827390717*^9, 3.559597884668641*^9, 3.559598279906232*^9, 
   3.559598543760419*^9, 3.559598597201508*^9, 3.5595986505628757`*^9},
 CellLabel->"Out[12]=",
 CellID->2110079741]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Once we have defined our objective we need our fitness function so that we \
can assign a fitness value to our algorithm. This fitness function counts the \
amount of True clauses in our function:\
\>", "MathCaption",
 CellChangeTimes->{{3.5595956640328197`*^9, 3.559595719233141*^9}, {
  3.5595959103093224`*^9, 3.559595912637446*^9}, {3.559595973256937*^9, 
  3.559595992399296*^9}, {3.559596119252861*^9, 3.559596138242176*^9}, {
  3.559596193131322*^9, 3.55959619373947*^9}, {3.559597217859529*^9, 
  3.5595973275552177`*^9}},
 CellID->1844993314],

Cell[BoxData[
 RowBox[{
  RowBox[{"EvaluateLogicPopulation", "[", 
   RowBox[{"function_", ",", "population_", ",", "x_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"CountTrues", "[", 
       RowBox[{"EvaluateConjunctionsList", "[", 
        RowBox[{"function", ",", 
         RowBox[{"population", "[", 
          RowBox[{"[", "i", "]"}], "]"}], ",", "x"}], "]"}], "]"}], 
      "\[IndentingNewLine]", ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", "population", "]"}]}], "}"}]}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 CellChangeTimes->{{3.5595985481674023`*^9, 3.559598548457594*^9}},
 CellLabel->"In[13]:=",
 CellID->1422686853]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
This time we will be using a uniform crossover operator with a binary flip \
mutation and a tournament parents selection scheme. So we define the \
parameters for the genetic algorithm:\
\>", "MathCaption",
 CellChangeTimes->{{3.5595956640328197`*^9, 3.559595719233141*^9}, {
  3.5595959103093224`*^9, 3.559595912637446*^9}, {3.559595973256937*^9, 
  3.559595992399296*^9}, {3.559596119252861*^9, 3.559596138242176*^9}, {
  3.559596193131322*^9, 3.55959619373947*^9}, {3.559597217859529*^9, 
  3.5595973275552177`*^9}, {3.5595974507525473`*^9, 3.5595975915528193`*^9}},
 CellID->1855386990],

Cell[BoxData[{
 RowBox[{
  RowBox[{"populationSize", "=", "30"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"parentsRoundSize", "=", "3"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"crossoverProbability", "=", ".65"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"crossoverUniformProbability", "=", ".1"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"mutationProbability", "=", ".002"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"generations", "=", "100"}], ";"}]}], "Input",
 CellChangeTimes->{{3.559597572137664*^9, 3.5595975753064528`*^9}, {
  3.55959762241507*^9, 3.559597675956793*^9}},
 CellLabel->"In[14]:=",
 CellID->1748496010],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"population", "=", 
    RowBox[{"GenerateRandomBinaryPopulation", "[", 
     RowBox[{"populationSize", ",", "numberOfVariables"}], "]"}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"dataArray", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", "0", "}"}], ",", 
     RowBox[{"{", "0", "}"}], ",", 
     RowBox[{"{", "0", "}"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"populationTemp", "=", "population"}], ";"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", "CycleStarts", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"output", "=", "\[IndentingNewLine]", 
   RowBox[{"Table", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{
      RowBox[{"populationLogic", "=", 
       RowBox[{"ConvertPopulationToLogic", "[", "population", "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"populationScores", "=", 
       RowBox[{"EvaluateLogicPopulation", "[", 
        RowBox[{"functionToAnalyze", ",", "populationLogic", ",", "x"}], 
        "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", "Reaping", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"reaping", "=", 
       RowBox[{"TournamentParentsSelection", "[", 
        RowBox[{
        "populationScores", ",", "parentsRoundSize", ",", "populationSize"}], 
        "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", "Crossover", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"crossoverPopulation", "=", 
       RowBox[{"CrossoverPopulationUniform", "[", 
        RowBox[{
        "populationTemp", ",", "reaping", ",", "crossoverProbability", ",", 
         "crossoverUniformProbability"}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", "Mutation", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"mutatedPopulation", "=", 
       RowBox[{"MutateBinaryPopulationFlip", "[", 
        RowBox[{"populationTemp", ",", "mutationProbability"}], "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", 
       RowBox[{
       "Data", " ", "Manipulation", " ", "and", " ", "Generational", " ", 
        "Replacement"}], "*)"}], "\[IndentingNewLine]", 
      RowBox[{"populationTemp", "=", "mutatedPopulation"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"dataArray", "=", 
       RowBox[{"AppendGenerationFitnessData", "[", 
        RowBox[{"dataArray", ",", "populationScores"}], "]"}]}], ";"}], 
     "\[IndentingNewLine]", ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "1", ",", "generations"}], "}"}]}], "]"}]}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.5595977363838167`*^9, 3.559597945778921*^9}, {
  3.559598001090844*^9, 3.5595981384685926`*^9}, {3.559598174460531*^9, 
  3.559598199254156*^9}, {3.559598243870611*^9, 3.5595982999689217`*^9}, {
  3.559598566103147*^9, 3.559598580790065*^9}, {3.55959868292244*^9, 
  3.5595987018685627`*^9}},
 CellLabel->"In[20]:=",
 CellID->806214522],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PlotSingleRunGA", "[", "dataArray", "]"}]], "Input",
 CellLabel->"In[28]:=",
 CellID->675480961],

Cell[BoxData[
 GraphicsBox[{
   {RGBColor[0, 0, 1], LineBox[CompressedData["
1:eJxdyzlKREEYhdGLkaGBgYGBioiISDvP9rOdZ23nVOjYLdTSakkuQaUFeaeg
+Dl83OmPz/5gJMng5//e4fvqpv2a4Zn5u6N4DI/jCTyJp7B3Fs/hebyAF/ES
7uBlvIJX8Rpexxt4E2/hbbyDd/Ee3scHuIubtgvOIR2nR8c5ouMc03FO6Din
dJwzOs45HeeCjnNJx7mi41zTcW7oOLd0nDs6zn3bDS644jywxwVXnD57XHDF
eWSPC644T+xxwRXnmT0uuOK8sMcFV5xX9rjgivPGHhdccd7Z//sbSahyzA==

     "]]}, 
   {RGBColor[0, 1, 0], LineBox[CompressedData["
1:eJxdyzlKhEEUhdGLkaGBgYGCioiIiPNs9+88T+2cCh27hVpabUTX4BK06UD6
PCiKw8ed+vjsdIeSdP9e7+/fTzuD13x/9W6i6XMYj+BRPIbH8SSexjN4Fs/h
ebyAF/ESXsYreBWv4XW8gTfxFt7GO3gX7+F93MJt3Ay64BzQcQ7pOEd0nGM6
zgkd55SOc0bHOafjXNBxLuk4V3ScazrODR3nlo5zR8e5H3SDC644D+xxwRWn
wx4XXHEe2eOCK84Te1xwxXlmjwuuOC/sccEV55U9LrjivLHHBVecd/b//gVp
u3dS
     "]]}, 
   {RGBColor[1, 0, 0], LineBox[CompressedData["
1:eJxdyzlKREEYhdGLkaGBgYGBioiISDvP9rOdZ23nVOjYLdTSakkuQaUFeaeg
+Dl83OmPz/5gJMng5//e4fvqpv2a4Zn4u6N4DI9j7ySewjN4Fs/hebyAF/ES
7uBlvIJX8Rpexxt4E2/hbbyDd/Ee3scHuIubtgvOIR2nR8c5ouMc03FO6Din
dJwzOs45HeeCjnNJx7mi41zTcW7oOLd0nDs6zn3bDS644jywxwVXnD57XHDF
eWSPC644T+xxwRXnmT0uuOK8sMcFV5xX9rjgivPGHhdccd7Z//sbK2Ru5A==

     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{0, Automatic},
  PlotRangeClipping->True]], "Output",
 CellChangeTimes->{{3.5595982200145187`*^9, 3.559598234349741*^9}, 
   3.559598273315946*^9, 3.559598306095456*^9, 3.5595986182474194`*^9},
 CellLabel->"Out[28]=",
 CellID->362244034]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "TutorialMoreAboutSection",
 CellID->23220180],

Cell["XXXX", "TutorialMoreAbout",
 CellID->1567025153]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Tutorials", "RelatedTutorialsSection",
 CellID->415694126],

Cell["XXXX", "RelatedTutorials",
 CellID->806871991]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Wolfram Education Group Courses", "TutorialRelatedLinksSection",
 CellID->415694148],

Cell["XXXX", "TutorialRelatedLinks",
 CellID->415694149]
}, Open  ]]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, 223}, {-4, Automatic}},
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 6, \
2010)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "TutorialPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 23, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 27, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 81, 2, 70, "Categorization",
 CellID->686433507],
Cell[1062, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1155, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1245, 53, 87, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1369, 59, 56, 1, 17, "KeywordsSection",
 CellID->1427428552],
Cell[1428, 62, 45, 1, 70, "Keywords",
 CellID->1251852827]
}, Closed]],
Cell[CellGroupData[{
Cell[1510, 68, 53, 1, 17, "DetailsSection",
 CellID->307771771],
Cell[1566, 71, 63, 2, 70, "Details",
 CellID->218895918],
Cell[1632, 75, 69, 2, 70, "Details",
 CellID->350963985],
Cell[1704, 79, 66, 2, 70, "Details",
 CellID->795871300],
Cell[1773, 83, 71, 2, 70, "Details",
 CellID->199739161],
Cell[1847, 87, 60, 2, 70, "Details",
 CellID->40625308],
Cell[1910, 91, 61, 2, 70, "Details",
 CellID->357121918],
Cell[1974, 95, 62, 2, 70, "Details",
 CellID->35949532],
Cell[2039, 99, 78, 2, 70, "Details",
 CellID->929432370],
Cell[2120, 103, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2224, 110, 52, 1, 104, "Title",
 CellID->509267359],
Cell[2279, 113, 349, 7, 36, "Text",
 CellID->1534169418],
Cell[2631, 122, 186, 5, 80, "DefinitionBox",
 CellID->2096742444],
Cell[2820, 129, 45, 1, 27, "Caption",
 CellID->1891092685],
Cell[CellGroupData[{
Cell[2890, 134, 946, 16, 121, "MathCaption",
 CellID->836781195],
Cell[3839, 152, 6843, 175, 639, "Input",
 CellID->2058623809]
}, Open  ]],
Cell[CellGroupData[{
Cell[10719, 332, 374, 7, 37, "MathCaption",
 CellID->677454377],
Cell[CellGroupData[{
Cell[11118, 343, 670, 13, 39, "Input",
 CellID->1679203177],
Cell[11791, 358, 1799, 56, 38, "Output",
 CellID->2110079741]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[13639, 420, 561, 10, 65, "MathCaption",
 CellID->1844993314],
Cell[14203, 432, 814, 21, 69, "Input",
 CellID->1422686853]
}, Open  ]],
Cell[CellGroupData[{
Cell[15054, 458, 600, 10, 65, "MathCaption",
 CellID->1855386990],
Cell[15657, 470, 689, 19, 99, "Input",
 CellID->1748496010],
Cell[16349, 491, 2927, 67, 354, "Input",
 CellID->806214522],
Cell[CellGroupData[{
Cell[19301, 562, 120, 3, 23, "Input",
 CellID->675480961],
Cell[19424, 567, 1464, 33, 128, "Output",
 CellID->362244034]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[20937, 606, 65, 1, 71, "TutorialMoreAboutSection",
 CellID->23220180],
Cell[21005, 609, 54, 1, 20, "TutorialMoreAbout",
 CellID->1567025153]
}, Open  ]],
Cell[CellGroupData[{
Cell[21096, 615, 72, 1, 71, "RelatedTutorialsSection",
 CellID->415694126],
Cell[21171, 618, 52, 1, 20, "RelatedTutorials",
 CellID->806871991]
}, Open  ]],
Cell[CellGroupData[{
Cell[21260, 624, 98, 1, 71, "TutorialRelatedLinksSection",
 CellID->415694148],
Cell[21361, 627, 56, 1, 20, "TutorialRelatedLinks",
 CellID->415694149]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
