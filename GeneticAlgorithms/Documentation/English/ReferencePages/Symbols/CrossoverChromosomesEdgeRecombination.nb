(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     12455,        502]
NotebookOptionsPosition[      8056,        344]
NotebookOutlinePosition[      8684,        369]
CellTagsIndexPosition[      8606,        364]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/ref/CrossoverEdgeRecombination", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["\<\
CrossoverChromosomesEdgeRecombination\
\>", "ObjectName",
 CellChangeTimes->{{3.558646680262121*^9, 3.558646681905467*^9}},
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"CrossoverChromosomesEdgeRecombination", "[", 
   RowBox[{
    StyleBox["chromosomeA", "TI"], ",", 
    StyleBox["chromosomeB", "TI"]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]CrossoverChromosomesEdgeRecombination applies a edge \
recombination crossover operation between the ",
 Cell[BoxData[
  StyleBox["chromosomeA", "TI"]], "InlineFormula"],
 " and ",
 Cell[BoxData[
  StyleBox["chromosomeB", "TI"]], "InlineFormula"],
 "."
}], "Usage",
 CellChangeTimes->{{3.558637332381094*^9, 3.5586373815408087`*^9}, {
  3.5586466843349323`*^9, 3.558646690153552*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\.1d\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.5586373836010933`*^9, 3.5586373915002747`*^9}},
 CellID->622678423],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CrossoverChromosomesEdgeRecombination", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "3", ",", "4", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "4", ",", "3", ",", "2", ",", "1"}], "}"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.5586373959778347`*^9, 3.558637409724536*^9}, {
  3.558646709169867*^9, 3.558646710869903*^9}},
 CellLabel->"In[9]:=",
 CellID->668443530],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"5", ",", "4", ",", "3", ",", "2", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "4", ",", "3", ",", "2", ",", "1"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.558637410260235*^9, 3.558646982663494*^9},
 CellLabel->"Out[11]=",
 CellID->1935073318]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CrossoverChromosomesEdgeRecombination", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "5", ",", "2", ",", "3", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "2", ",", "3", ",", "4", ",", "1"}], "}"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.55863741972508*^9, 3.5586374275896997`*^9}, {
  3.5586467127813377`*^9, 3.5586467141579*^9}},
 CellLabel->"In[12]:=",
 CellID->933747769],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "4", ",", "3", ",", "2", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4", ",", "3", ",", "2", ",", "5"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.558637428037422*^9, 3.5586469841678123`*^9},
 CellLabel->"Out[12]=",
 CellID->1284905458]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 6, \
2010)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[6536, 286, 100, 2, 53, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 8468, 357}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 23, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 27, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1153, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1243, 53, 93, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1373, 59, 55, 1, 17, "KeywordsSection",
 CellID->477174294],
Cell[1431, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1513, 68, 65, 1, 17, "TemplatesSection",
 CellID->1872225408],
Cell[1581, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1678, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1763, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1847, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1966, 90, 53, 1, 17, "DetailsSection",
 CellID->307771771],
Cell[2022, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2088, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2160, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2227, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2299, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2363, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2427, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2493, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2574, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2678, 132, 154, 4, 62, "ObjectName",
 CellID->1224892054],
Cell[2835, 138, 652, 18, 86, "Usage",
 CellID->982511436],
Cell[3490, 158, 42, 1, 23, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3569, 164, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3629, 167, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3711, 173, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3797, 176, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3892, 182, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3960, 185, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[4046, 191, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[4104, 194, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[4184, 200, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[4244, 203, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4327, 209, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[4686, 222, 177, 3, 23, "Input",
 CellID->622678423],
Cell[CellGroupData[{
Cell[4888, 229, 434, 11, 23, "Input",
 CellID->668443530],
Cell[5325, 242, 343, 10, 22, "Output",
 CellID->1935073318]
}, Open  ]],
Cell[CellGroupData[{
Cell[5705, 257, 434, 11, 23, "Input",
 CellID->933747769],
Cell[6142, 270, 345, 10, 22, "Output",
 CellID->1284905458]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6536, 286, 100, 2, 53, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[6639, 290, 125, 3, 31, "ExampleSection",
 CellID->1293636265],
Cell[6767, 295, 148, 3, 19, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[6940, 302, 127, 3, 19, "ExampleSection",
 CellID->2061341341],
Cell[7070, 307, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[7203, 312, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[7348, 318, 131, 3, 19, "ExampleSection",
 CellID->258228157],
Cell[7482, 323, 142, 3, 19, "ExampleSection",
 CellID->2123667759],
Cell[7627, 328, 135, 3, 19, "ExampleSection",
 CellID->1305812373],
Cell[7765, 333, 140, 3, 19, "ExampleSection",
 CellID->1653164318],
Cell[7908, 338, 132, 3, 19, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
