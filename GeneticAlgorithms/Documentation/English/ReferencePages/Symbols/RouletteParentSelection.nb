(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     10821,        447]
NotebookOptionsPosition[      6575,        295]
NotebookOutlinePosition[      7205,        320]
CellTagsIndexPosition[      7127,        315]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/ref/RouletteParentSelection", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["RouletteParentSelection", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"RouletteParentSelection", "[", 
   StyleBox["fitnessesArray", "TI"], "]"}]], "InlineFormula"],
 " \[LineSeparator]RouletteParentSelection returns the index of the parent \
selected for crossover operations according to the ",
 Cell[BoxData[
  StyleBox["fitnessesArray", "TI"]], "InlineFormula"],
 " according to the roulette parents selection algorithm."
}], "Usage",
 CellChangeTimes->{{3.5586455045726547`*^9, 3.558645548232585*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\>\"", "]"}]], "Input",
 CellID->554362544],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"RouletteParentSelection", "[", 
  RowBox[{"{", 
   RowBox[{"10", ",", "5", ",", "6", ",", "12", ",", "9"}], "}"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.558991303778829*^9, 3.558991321779924*^9}},
 CellLabel->"In[22]:=",
 CellID->1950618178],

Cell[BoxData["5"], "Output",
 CellChangeTimes->{{3.558991322322332*^9, 3.5589913271203337`*^9}},
 CellLabel->"Out[22]=",
 CellID->2016527861]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{131, Automatic}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 6, \
2010)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[5055, 237, 100, 2, 53, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 6989, 308}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 23, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 27, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1153, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1243, 53, 90, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1370, 59, 55, 1, 17, "KeywordsSection",
 CellID->477174294],
Cell[1428, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1510, 68, 65, 1, 17, "TemplatesSection",
 CellID->1872225408],
Cell[1578, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1675, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1760, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1844, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1963, 90, 53, 1, 17, "DetailsSection",
 CellID->307771771],
Cell[2019, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2085, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2157, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2224, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2296, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2360, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2424, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2490, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2571, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2675, 132, 66, 1, 62, "ObjectName",
 CellID->1224892054],
Cell[2744, 135, 522, 12, 86, "Usage",
 CellID->982511436],
Cell[3269, 149, 42, 1, 23, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3348, 155, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3408, 158, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3490, 164, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3576, 167, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3671, 173, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3739, 176, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[3825, 182, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[3883, 185, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[3963, 191, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[4023, 194, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4106, 200, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[4465, 213, 103, 2, 23, "Input",
 CellID->554362544],
Cell[CellGroupData[{
Cell[4593, 219, 269, 7, 23, "Input",
 CellID->1950618178],
Cell[4865, 228, 141, 3, 22, "Output",
 CellID->2016527861]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5055, 237, 100, 2, 53, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[5158, 241, 125, 3, 31, "ExampleSection",
 CellID->1293636265],
Cell[5286, 246, 148, 3, 19, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[5459, 253, 127, 3, 19, "ExampleSection",
 CellID->2061341341],
Cell[5589, 258, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[5722, 263, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[5867, 269, 131, 3, 19, "ExampleSection",
 CellID->258228157],
Cell[6001, 274, 142, 3, 19, "ExampleSection",
 CellID->2123667759],
Cell[6146, 279, 135, 3, 19, "ExampleSection",
 CellID->1305812373],
Cell[6284, 284, 140, 3, 19, "ExampleSection",
 CellID->1653164318],
Cell[6427, 289, 132, 3, 19, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
