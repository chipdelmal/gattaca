(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     13260,        526]
NotebookOptionsPosition[      8715,        362]
NotebookOutlinePosition[      9343,        387]
CellTagsIndexPosition[      9265,        382]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/ref/MutateBinaryChromosomeFlip", "Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["MutateBinaryChromosomeFlip", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"MutateBinaryChromosomeFlip", "[", 
   RowBox[{
    StyleBox["chromosome", "TI"], ",", 
    StyleBox["probability", "TI"]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]MutateBinaryChromosomeFlip applies a binary flip mutation \
to an input ",
 Cell[BoxData[
  StyleBox["chromosome", "TI"]], "InlineFormula"],
 " with a certain amount of ",
 Cell[BoxData[
  StyleBox["probability", "TI"]], "InlineFormula"],
 " per allele."
}], "Usage",
 CellChangeTimes->{{3.5582963760929613`*^9, 3.55829639865576*^9}, {
   3.558296443614133*^9, 3.558296483866241*^9}, 3.558296585731956*^9, {
   3.55837518401492*^9, 3.558375189171606*^9}},
 CellID->982511436],

Cell["\<\
The \"Binary Flip Mutation\" operation worsk over a binary chromosome and is \
applied to every allele of a chromosome by generating a random number and \
evaluating it. If it falls below the probability parameter then the allele \
has its value flipped to the other binary value allowed.\
\>", "Notes",
 CellChangeTimes->{{3.559585393771882*^9, 3.559585489497294*^9}},
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\>\"", "]"}]], "Input",
 CellID->1531889852],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MutateBinaryChromosomeFlip", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "1", ",", "0", ",", "1", ",", "1", ",", "0", ",", "1", ",", "1", ",", "0",
      ",", "1"}], "}"}], ",", ".9"}], "]"}]], "Input",
 CellChangeTimes->{{3.5582965943158197`*^9, 3.5582966164510593`*^9}},
 CellLabel->"In[17]:=",
 CellID->652071896],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0", ",", "1", ",", "0", ",", "0", ",", "1", ",", "0", ",", "0", ",", "1", 
   ",", "0"}], "}"}]], "Output",
 CellChangeTimes->{{3.558296607647217*^9, 3.558296617136712*^9}, {
  3.5756613273747177`*^9, 3.575661331543775*^9}},
 CellLabel->"Out[17]=",
 CellID->947791800]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MutateBinaryChromosomeFlip", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1",
      ",", "1"}], "}"}], ",", ".2"}], "]"}]], "Input",
 CellChangeTimes->{{3.5582965943158197`*^9, 3.5582966352927933`*^9}},
 CellLabel->"In[20]:=",
 CellID->153343643],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0", ",", "0", ",", "0", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", 
   ",", "1"}], "}"}]], "Output",
 CellChangeTimes->{{3.558296607647217*^9, 3.558296639761382*^9}, {
  3.575661334314999*^9, 3.575661344599457*^9}},
 CellLabel->"Out[20]=",
 CellID->886503784]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MutateBinaryChromosomeFlip", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
    "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1",
      ",", "1"}], "}"}], ",", ".5"}], "]"}]], "Input",
 CellChangeTimes->{{3.558296646118984*^9, 3.558296646213361*^9}},
 CellLabel->"In[19]:=",
 CellID->119889547],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "1", ",", "0", ",", "1", ",", "1", ",", "0", ",", "1", ",", "1", ",", "1", 
   ",", "1"}], "}"}]], "Output",
 CellChangeTimes->{3.558296646737393*^9, 3.575661337120646*^9},
 CellLabel->"Out[19]=",
 CellID->2130798108]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{4, Automatic}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[7195, 304, 100, 2, 55, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 9127, 375}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 24, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 29, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1153, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1243, 53, 93, 1, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1373, 59, 55, 1, 19, "KeywordsSection",
 CellID->477174294],
Cell[1431, 62, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1513, 68, 65, 1, 19, "TemplatesSection",
 CellID->1872225408],
Cell[1581, 71, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1678, 75, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1763, 79, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1847, 83, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1966, 90, 53, 1, 19, "DetailsSection",
 CellID->307771771],
Cell[2022, 93, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2088, 97, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2160, 101, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2227, 105, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2299, 109, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2363, 113, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2427, 117, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2493, 121, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2574, 125, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2678, 132, 69, 1, 63, "ObjectName",
 CellID->1224892054],
Cell[2750, 135, 711, 19, 91, "Usage",
 CellID->982511436],
Cell[3464, 156, 400, 7, 57, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3901, 168, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3961, 171, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[4043, 177, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[4129, 180, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[4224, 186, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[4292, 189, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[4378, 195, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[4436, 198, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[4516, 204, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[4576, 207, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4659, 213, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[5018, 226, 104, 2, 25, "Input",
 CellID->1531889852],
Cell[CellGroupData[{
Cell[5147, 232, 347, 9, 25, "Input",
 CellID->652071896],
Cell[5497, 243, 311, 8, 23, "Output",
 CellID->947791800]
}, Open  ]],
Cell[CellGroupData[{
Cell[5845, 256, 347, 9, 25, "Input",
 CellID->153343643],
Cell[6195, 267, 309, 8, 23, "Output",
 CellID->886503784]
}, Open  ]],
Cell[CellGroupData[{
Cell[6541, 280, 343, 9, 25, "Input",
 CellID->119889547],
Cell[6887, 291, 259, 7, 23, "Output",
 CellID->2130798108]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7195, 304, 100, 2, 55, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[7298, 308, 125, 3, 33, "ExampleSection",
 CellID->1293636265],
Cell[7426, 313, 148, 3, 21, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[7599, 320, 127, 3, 21, "ExampleSection",
 CellID->2061341341],
Cell[7729, 325, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[7862, 330, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[8007, 336, 131, 3, 21, "ExampleSection",
 CellID->258228157],
Cell[8141, 341, 142, 3, 21, "ExampleSection",
 CellID->2123667759],
Cell[8286, 346, 135, 3, 21, "ExampleSection",
 CellID->1305812373],
Cell[8424, 351, 140, 3, 21, "ExampleSection",
 CellID->1653164318],
Cell[8567, 356, 132, 3, 21, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
