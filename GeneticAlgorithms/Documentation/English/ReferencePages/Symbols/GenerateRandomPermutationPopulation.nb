(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     12724,        507]
NotebookOptionsPosition[      8322,        349]
NotebookOutlinePosition[      8952,        374]
CellTagsIndexPosition[      8874,        369]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 "New in: ",
 Cell["0.0", "HistoryData",
  CellTags->"New"],
 " | Modified in: ",
 Cell[" ", "HistoryData",
  CellTags->"Modified"],
 " | Obsolete in: ",
 Cell[" ", "HistoryData",
  CellTags->"Obsolete"],
 " | Excised in: ",
 Cell[" ", "HistoryData",
  CellTags->"Excised"]
}], "History",
 CellID->1247902091],

Cell[CellGroupData[{

Cell["Categorization", "CategorizationSection",
 CellID->1122911449],

Cell["Symbol", "Categorization",
 CellLabel->"Entity Type",
 CellID->686433507],

Cell["GeneticAlgorithms", "Categorization",
 CellLabel->"Paclet Name",
 CellID->605800465],

Cell["GeneticAlgorithms`", "Categorization",
 CellLabel->"Context",
 CellID->468444828],

Cell["GeneticAlgorithms/ref/GenerateRandomPermutationPopulation", \
"Categorization",
 CellLabel->"URI"]
}, Closed]],

Cell[CellGroupData[{

Cell["Keywords", "KeywordsSection",
 CellID->477174294],

Cell["XXXX", "Keywords",
 CellID->1164421360]
}, Closed]],

Cell[CellGroupData[{

Cell["Syntax Templates", "TemplatesSection",
 CellID->1872225408],

Cell[BoxData[""], "Template",
 CellLabel->"Additional Function Template",
 CellID->1562036412],

Cell[BoxData[""], "Template",
 CellLabel->"Arguments Pattern",
 CellID->158391909],

Cell[BoxData[""], "Template",
 CellLabel->"Local Variables",
 CellID->1360575930],

Cell[BoxData[""], "Template",
 CellLabel->"Color Equal Signs",
 CellID->793782254]
}, Closed]],

Cell[CellGroupData[{

Cell["Details", "DetailsSection",
 CellID->307771771],

Cell["XXXX", "Details",
 CellLabel->"Lead",
 CellID->670882175],

Cell["XXXX", "Details",
 CellLabel->"Developers",
 CellID->350963985],

Cell["XXXX", "Details",
 CellLabel->"Authors",
 CellID->8391405],

Cell["XXXX", "Details",
 CellLabel->"Feature Name",
 CellID->3610269],

Cell["XXXX", "Details",
 CellLabel->"QA",
 CellID->401364205],

Cell["XXXX", "Details",
 CellLabel->"DA",
 CellID->350204745],

Cell["XXXX", "Details",
 CellLabel->"Docs",
 CellID->732958810],

Cell["XXXX", "Details",
 CellLabel->"Features Page Notes",
 CellID->222905350],

Cell["XXXX", "Details",
 CellLabel->"Comments",
 CellID->240026365]
}, Closed]],

Cell[CellGroupData[{

Cell["GenerateRandomPermutationPopulation", "ObjectName",
 CellID->1224892054],

Cell[TextData[{
 Cell["   ", "ModInfo"],
 Cell[BoxData[
  RowBox[{"GenerateRandomPermutationPopulation", "[", 
   RowBox[{
    StyleBox["populationSize", "TI"], ",", 
    StyleBox["chromosomeSize", "TI"]}], "]"}]], "InlineFormula"],
 " \[LineSeparator]GenerateRandomPermutationPopulation creates a permutations \
population of a size defined by ",
 Cell[BoxData[
  StyleBox["populationSize", "TI"]], "InlineFormula"],
 " and in which every individual has a size defined by ",
 Cell[BoxData[
  StyleBox["chromosomeSize", "TI"]], "InlineFormula"],
 "."
}], "Usage",
 CellChangeTimes->{{3.55826343454526*^9, 3.558263469612586*^9}},
 CellID->982511436],

Cell["XXXX", "Notes",
 CellID->1067943069]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tutorials", "TutorialsSection",
 CellID->250839057],

Cell["XXXX", "Tutorials",
 CellID->341631938]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Demonstrations", "RelatedDemonstrationsSection",
 CellID->1268215905],

Cell["XXXX", "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],

Cell[CellGroupData[{

Cell["Related Links", "RelatedLinksSection",
 CellID->1584193535],

Cell["XXXX", "RelatedLinks",
 CellID->1038487239]
}, Open  ]],

Cell[CellGroupData[{

Cell["See Also", "SeeAlsoSection",
 CellID->1255426704],

Cell["XXXX", "SeeAlso",
 CellID->929782353]
}, Open  ]],

Cell[CellGroupData[{

Cell["More About", "MoreAboutSection",
 CellID->38303248],

Cell["XXXX", "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[GridBox[{
    {
     StyleBox["Examples", "PrimaryExamplesSection"], 
     ButtonBox[
      RowBox[{
       RowBox[{"More", " ", "Examples"}], " ", "\[RightTriangle]"}],
      BaseStyle->"ExtendedExamplesLink",
      ButtonData:>"ExtendedExamples"]}
   }],
  $Line = 0; Null]], "PrimaryExamplesSection",
 CellID->880084151],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\>\"", "]"}]], "Input",
 CellID->1956322770],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"GenerateRandomPermutationPopulation", "[", 
  RowBox[{"10", ",", "5"}], "]"}]], "Input",
 CellChangeTimes->{{3.558263480685443*^9, 3.558263491392074*^9}},
 CellLabel->"In[21]:=",
 CellID->1942159820],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"4", ",", "3", ",", "1", ",", "5", ",", "2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "5", ",", "2", ",", "3", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "5", ",", "3", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"3", ",", "5", ",", "4", ",", "2", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "5", ",", "4", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"3", ",", "5", ",", "4", ",", "1", ",", "2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"2", ",", "1", ",", "4", ",", "3", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"4", ",", "3", ",", "5", ",", "2", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "3", ",", "2", ",", "4", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "4", ",", "1", ",", "3", ",", "2"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.558263491889669*^9},
 CellLabel->"Out[21]=",
 CellID->128660340]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"GenerateRandomPermutationPopulation", "[", 
  RowBox[{"5", ",", "3"}], "]"}]], "Input",
 CellChangeTimes->{{3.558263480685443*^9, 3.558263499479875*^9}},
 CellLabel->"In[22]:=",
 CellID->2025785498],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"3", ",", "2", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"2", ",", "3", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"3", ",", "2", ",", "1"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.5582634998668823`*^9},
 CellLabel->"Out[22]=",
 CellID->1171125348]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["More Examples", "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],

Cell[BoxData[
 InterpretationBox[Cell["Scope", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1293636265],

Cell[BoxData[
 InterpretationBox[Cell["Generalizations & Extensions", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1020263627],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[Cell["Options", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2061341341],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1757724783],

Cell[BoxData[
 InterpretationBox[Cell["XXXX", "ExampleSubsection"],
  $Line = 0; Null]], "ExampleSubsection",
 CellID->1295379749]
}, Closed]],

Cell[BoxData[
 InterpretationBox[Cell["Applications", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->258228157],

Cell[BoxData[
 InterpretationBox[Cell["Properties & Relations", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->2123667759],

Cell[BoxData[
 InterpretationBox[Cell["Possible Issues", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1305812373],

Cell[BoxData[
 InterpretationBox[Cell["Interactive Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->1653164318],

Cell[BoxData[
 InterpretationBox[Cell["Neat Examples", "ExampleSection"],
  $Line = 0; Null]], "ExampleSection",
 CellID->589267740]
}, Open  ]]
},
WindowSize->{700, 770},
WindowMargins->{{Automatic, 239}, {Automatic, 0}},
CellContext->"Global`",
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (November 6, \
2010)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "FunctionPageStyles.nb", 
  CharacterEncoding -> "UTF-8"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "ExtendedExamples"->{
  Cell[6802, 291, 100, 2, 53, "ExtendedExamplesSection",
   CellTags->"ExtendedExamples",
   CellID->1854448968]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"ExtendedExamples", 8736, 362}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 325, 14, 23, "History",
 CellID->1247902091],
Cell[CellGroupData[{
Cell[907, 38, 68, 1, 27, "CategorizationSection",
 CellID->1122911449],
Cell[978, 41, 79, 2, 70, "Categorization",
 CellID->686433507],
Cell[1060, 45, 90, 2, 70, "Categorization",
 CellID->605800465],
Cell[1153, 49, 87, 2, 70, "Categorization",
 CellID->468444828],
Cell[1243, 53, 104, 2, 70, "Categorization"]
}, Closed]],
Cell[CellGroupData[{
Cell[1384, 60, 55, 1, 17, "KeywordsSection",
 CellID->477174294],
Cell[1442, 63, 45, 1, 70, "Keywords",
 CellID->1164421360]
}, Closed]],
Cell[CellGroupData[{
Cell[1524, 69, 65, 1, 17, "TemplatesSection",
 CellID->1872225408],
Cell[1592, 72, 94, 2, 70, "Template",
 CellID->1562036412],
Cell[1689, 76, 82, 2, 70, "Template",
 CellID->158391909],
Cell[1774, 80, 81, 2, 70, "Template",
 CellID->1360575930],
Cell[1858, 84, 82, 2, 70, "Template",
 CellID->793782254]
}, Closed]],
Cell[CellGroupData[{
Cell[1977, 91, 53, 1, 17, "DetailsSection",
 CellID->307771771],
Cell[2033, 94, 63, 2, 70, "Details",
 CellID->670882175],
Cell[2099, 98, 69, 2, 70, "Details",
 CellID->350963985],
Cell[2171, 102, 64, 2, 70, "Details",
 CellID->8391405],
Cell[2238, 106, 69, 2, 70, "Details",
 CellID->3610269],
Cell[2310, 110, 61, 2, 70, "Details",
 CellID->401364205],
Cell[2374, 114, 61, 2, 70, "Details",
 CellID->350204745],
Cell[2438, 118, 63, 2, 70, "Details",
 CellID->732958810],
Cell[2504, 122, 78, 2, 70, "Details",
 CellID->222905350],
Cell[2585, 126, 67, 2, 70, "Details",
 CellID->240026365]
}, Closed]],
Cell[CellGroupData[{
Cell[2689, 133, 78, 1, 62, "ObjectName",
 CellID->1224892054],
Cell[2770, 136, 648, 17, 86, "Usage",
 CellID->982511436],
Cell[3421, 155, 42, 1, 23, "Notes",
 CellID->1067943069]
}, Open  ]],
Cell[CellGroupData[{
Cell[3500, 161, 57, 1, 43, "TutorialsSection",
 CellID->250839057],
Cell[3560, 164, 45, 1, 16, "Tutorials",
 CellID->341631938]
}, Open  ]],
Cell[CellGroupData[{
Cell[3642, 170, 83, 1, 30, "RelatedDemonstrationsSection",
 CellID->1268215905],
Cell[3728, 173, 58, 1, 16, "RelatedDemonstrations",
 CellID->1129518860]
}, Open  ]],
Cell[CellGroupData[{
Cell[3823, 179, 65, 1, 30, "RelatedLinksSection",
 CellID->1584193535],
Cell[3891, 182, 49, 1, 16, "RelatedLinks",
 CellID->1038487239]
}, Open  ]],
Cell[CellGroupData[{
Cell[3977, 188, 55, 1, 30, "SeeAlsoSection",
 CellID->1255426704],
Cell[4035, 191, 43, 1, 16, "SeeAlso",
 CellID->929782353]
}, Open  ]],
Cell[CellGroupData[{
Cell[4115, 197, 57, 1, 30, "MoreAboutSection",
 CellID->38303248],
Cell[4175, 200, 46, 1, 16, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[CellGroupData[{
Cell[4258, 206, 356, 11, 69, "PrimaryExamplesSection",
 CellID->880084151],
Cell[4617, 219, 104, 2, 23, "Input",
 CellID->1956322770],
Cell[CellGroupData[{
Cell[4746, 225, 223, 5, 23, "Input",
 CellID->1942159820],
Cell[4972, 232, 1032, 26, 38, "Output",
 CellID->128660340]
}, Open  ]],
Cell[CellGroupData[{
Cell[6041, 263, 222, 5, 23, "Input",
 CellID->2025785498],
Cell[6266, 270, 487, 15, 22, "Output",
 CellID->1171125348]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6802, 291, 100, 2, 53, "ExtendedExamplesSection",
 CellTags->"ExtendedExamples",
 CellID->1854448968],
Cell[6905, 295, 125, 3, 31, "ExampleSection",
 CellID->1293636265],
Cell[7033, 300, 148, 3, 19, "ExampleSection",
 CellID->1020263627],
Cell[CellGroupData[{
Cell[7206, 307, 127, 3, 19, "ExampleSection",
 CellID->2061341341],
Cell[7336, 312, 130, 3, 70, "ExampleSubsection",
 CellID->1757724783],
Cell[7469, 317, 130, 3, 70, "ExampleSubsection",
 CellID->1295379749]
}, Closed]],
Cell[7614, 323, 131, 3, 19, "ExampleSection",
 CellID->258228157],
Cell[7748, 328, 142, 3, 19, "ExampleSection",
 CellID->2123667759],
Cell[7893, 333, 135, 3, 19, "ExampleSection",
 CellID->1305812373],
Cell[8031, 338, 140, 3, 19, "ExampleSection",
 CellID->1653164318],
Cell[8174, 343, 132, 3, 19, "ExampleSection",
 CellID->589267740]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
