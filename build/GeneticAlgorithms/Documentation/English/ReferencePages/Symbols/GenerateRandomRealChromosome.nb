(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      9640,        311]
NotebookOptionsPosition[      6942,        227]
NotebookOutlinePosition[      8701,        276]
CellTagsIndexPosition[      8617,        271]
WindowTitle->GenerateRandomRealChromosome - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 StyleBox[ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:guide/Mathematica"],
  FontSlant->"Italic"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["GENETICALGORITHMS PACLET SYMBOL", "PacletNameCell"], Cell[
    TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"More About \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"Genetic Algorithms Package\"\>":>
       Documentation`HelpLookup["paclet:GeneticAlgorithms/guide/Main"]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"URL \[RightGuillemet]\"\>",
        StripOnInput->
         False], {"\<\"GeneticAlgorithms/ref/GenerateRandonRealChromosome\"\>\
":>None, "\<\"Copy Mathematica url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell["GeneticAlgorithms/ref/GenerateRandonRealChromosome"]}, 
            Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; Null], 
       Delimiter, "\<\"Copy web url\"\>":>
       Module[{DocumentationSearch`Private`nb$}, 
        DocumentationSearch`Private`nb$ = NotebookPut[
           Notebook[{
             Cell[
              BoxData[
               MakeBoxes[
                Hyperlink[
                "http://reference.wolfram.com/mathematica/GeneticAlgorithms/\
ref/GenerateRandonRealChromosome.html"], StandardForm]], "Input", 
              TextClipboardType -> "PlainText"]}, Visible -> False]]; 
        SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
        FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
        NotebookClose[DocumentationSearch`Private`nb$]; 
        Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
         NotebookLocate[{
           URL[
            StringJoin[
            "http://reference.wolfram.com/mathematica/", 
             "GeneticAlgorithms/ref/GenerateRandonRealChromosome", ".html"]], 
           None}]}]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0}]
    }], "AnchorBar"]}
  }]], "AnchorBarGrid",
 GridBoxOptions->{GridBoxItemSize->{"Columns" -> {
     Scaled[0.65], {
      Scaled[0.34]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
   "RowsIndexed" -> {}}},
 CellID->1],

Cell["GenerateRandomRealChromosome", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"GenerateRandomRealChromosome", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{
          StyleBox["xmin", "TI"], ",", 
          StyleBox["xmax", "TI"]}], "}"}], ",", 
        StyleBox["size", "TI"]}], "]"}]], "InlineFormula"],
     " \[LineSeparator]Generates a real values chromosome within the limits \
of ",
     Cell[BoxData[
      StyleBox[
       RowBox[{"xmin", " "}], "TI"]], "InlineFormula"],
     "and ",
     Cell[BoxData[
      StyleBox["xmax", "TI"]], "InlineFormula"],
     " and with a determined ",
     Cell[BoxData[
      StyleBox["size", "TI"]], "InlineFormula"],
     "."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1443734109],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->421442510],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GenericAlgorithms`\.1d\>\"", "]"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->175749262],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"GenerateRandomRealChromosome", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], ",", "5"}], "]"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->1027831809],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "1.4281807478076027`", ",", "2.8668976306536487`", ",", 
   "2.4545584615484097`", ",", "2.953504250608696`", ",", 
   "2.4625507920822276`"}], "}"}]], "Output",
 ImageSize->{298, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[2]=",
 CellID->1587096307]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"GenerateRandomRealChromosome", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0", ",", "10"}], "}"}], ",", "5"}], "]"}]], "Input",
 CellLabel->"In[3]:=",
 CellID->433252362],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "8.674035025330763`", ",", "9.939205585243144`", ",", "5.89853220163263`", 
   ",", "0.10575503039329526`", ",", "3.1103249962847723`"}], "}"}]], "Output",\

 ImageSize->{320, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[3]=",
 CellID->1702884804]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["MORE ABOUT",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "MoreAboutSection",
 CellID->38303248],

Cell[TextData[ButtonBox["Genetic Algorithms Package",
 BaseStyle->"Link",
 ButtonData->"paclet:GeneticAlgorithms/guide/Main"]], "MoreAbout",
 CellID->1665078683]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"GenerateRandomRealChromosome - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2013, 12, 18, 7, 22, 31.154112}", "context" -> 
    "GeneticAlgorithms`", "keywords" -> {}, "index" -> True, "label" -> 
    "GeneticAlgorithms Paclet Symbol", "language" -> "en", "paclet" -> 
    "GeneticAlgorithms", "status" -> "None", "summary" -> 
    "GenerateRandomRealChromosome[{xmin, xmax}, size] Generates a real values \
chromosome within the limits of xmin and xmax and with a determined size.", 
    "synonyms" -> {}, "title" -> "GenerateRandomRealChromosome", "type" -> 
    "Symbol", "uri" -> "GeneticAlgorithms/ref/GenerateRandonRealChromosome"}, 
  "LinkTrails" -> "", "SearchTextTranslated" -> ""},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> 
  "9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, 2013)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[4360, 126, 462, 13, 70, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1443734109]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 8473, 264}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[621, 21, 76, 1, 70, "SymbolColorBar"],
Cell[700, 24, 209, 6, 70, "LinkTrail"],
Cell[912, 32, 2477, 57, 70, "AnchorBarGrid",
 CellID->1],
Cell[3392, 91, 71, 1, 70, "ObjectName",
 CellID->1224892054],
Cell[3466, 94, 869, 28, 70, "Usage",
 CellID->982511436],
Cell[CellGroupData[{
Cell[4360, 126, 462, 13, 70, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1443734109],
Cell[CellGroupData[{
Cell[4847, 143, 148, 5, 70, "ExampleSection",
 CellID->421442510],
Cell[4998, 150, 130, 3, 70, "Input",
 CellID->175749262],
Cell[CellGroupData[{
Cell[5153, 157, 197, 6, 70, "Input",
 CellID->1027831809],
Cell[5353, 165, 335, 10, 34, "Output",
 CellID->1587096307]
}, Open  ]],
Cell[CellGroupData[{
Cell[5725, 180, 197, 6, 70, "Input",
 CellID->433252362],
Cell[5925, 188, 331, 10, 34, "Output",
 CellID->1702884804]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6317, 205, 419, 12, 70, "MoreAboutSection",
 CellID->38303248],
Cell[6739, 219, 161, 3, 70, "MoreAbout",
 CellID->1665078683]
}, Open  ]],
Cell[6915, 225, 23, 0, 70, "FooterCell"]
}
]
*)

(* End of internal cache information *)

