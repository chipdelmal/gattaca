(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      7854,        240]
NotebookOptionsPosition[      5349,        166]
NotebookOutlinePosition[      7233,        217]
CellTagsIndexPosition[      7149,        212]
WindowTitle->CrossoverDifferentialEvolution - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:GeneticAlgorithms/guide/GeneticAlgorithms"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["GENETICALGORITHMS PACLET SYMBOL", "PacletNameCell"], Cell[TextData[
    Cell[BoxData[
     ActionMenuBox[
      FrameBox["\<\"URL \[RightGuillemet]\"\>",
       StripOnInput->
        False], {"\<\"GeneticAlgorithms/ref/CrossoverDifferentialEvolution\"\>\
":>None, "\<\"Copy Mathematica url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell["GeneticAlgorithms/ref/CrossoverDifferentialEvolution"]}, 
           Visible -> False]]; 
       SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; Null], 
      Delimiter, "\<\"Copy web url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell[
             BoxData[
              MakeBoxes[
               Hyperlink[
               "http://reference.wolfram.com/mathematica/GeneticAlgorithms/\
ref/CrossoverDifferentialEvolution.html"], StandardForm]], "Input", 
             TextClipboardType -> "PlainText"]}, Visible -> False]]; 
       SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; 
       Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
        NotebookLocate[{
          URL[
           StringJoin[
           "http://reference.wolfram.com/mathematica/", 
            "GeneticAlgorithms/ref/CrossoverDifferentialEvolution", ".html"]],
           None}]}]},
      Appearance->None,
      MenuAppearance->Automatic]],
     LineSpacing->{1.4, 0}]], "AnchorBar"]}
  }]], "AnchorBarGrid",
 GridBoxOptions->{GridBoxItemSize->{"Columns" -> {
     Scaled[0.65], {
      Scaled[0.34]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
   "RowsIndexed" -> {}}},
 CellID->1],

Cell["CrossoverDifferentialEvolution", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"CrossoverDifferentialEvolution", "[", 
       RowBox[{
        StyleBox["originalVector", "TI"], ",", 
        StyleBox["mutatedVector", "TI"], ",", 
        StyleBox["crossoverConstant", "TI"]}], "]"}]], "InlineFormula"],
     " \[LineSeparator]CrossoverDifferentialEvolution applies a crossover \
operation between an ",
     Cell[BoxData[
      StyleBox["originalVector", "TI"]], "InlineFormula"],
     " and a ",
     Cell[BoxData[
      StyleBox["mutatedVector", "TI"]], "InlineFormula"],
     " with a given ",
     Cell[BoxData[
      StyleBox["crossoverConstant", "TI"]], "InlineFormula"],
     " for a differential evolution algorithm."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1938052926],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->1971053233],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\>\"", "]"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->582053094],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CrossoverDifferentialEvolution", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "3", ",", "4", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"6", ",", "7", ",", "8", ",", "9", ",", "0"}], "}"}], ",", ".5"}],
   "]"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->778338773],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"6", ",", "7", ",", "3", ",", "9", ",", "0"}], "}"}]], "Output",
 ImageSize->{97, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[2]=",
 CellID->1742764788]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"CrossoverDifferentialEvolution - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2013, 12, 18, 7, 22, 22.228822}", "context" -> 
    "GeneticAlgorithms`", "keywords" -> {}, "index" -> True, "label" -> 
    "GeneticAlgorithms Paclet Symbol", "language" -> "en", "paclet" -> 
    "GeneticAlgorithms", "status" -> "None", "summary" -> 
    "CrossoverDifferentialEvolution[originalVector, mutatedVector, \\ \
crossoverConstant] CrossoverDifferentialEvolution applies a crossover \
operation between an originalVector and a mutatedVector with a given \
crossoverConstant for a differential evolution algorithm.", "synonyms" -> {}, 
    "title" -> "CrossoverDifferentialEvolution", "type" -> "Symbol", "uri" -> 
    "GeneticAlgorithms/ref/CrossoverDifferentialEvolution"}, "LinkTrails" -> 
  "", "SearchTextTranslated" -> ""},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> 
  "9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, 2013)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[3935, 110, 462, 13, 70, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1938052926]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 7005, 205}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[623, 21, 76, 1, 70, "SymbolColorBar"],
Cell[702, 24, 200, 5, 70, "LinkTrail"],
Cell[905, 31, 2018, 45, 70, "AnchorBarGrid",
 CellID->1],
Cell[2926, 78, 73, 1, 70, "ObjectName",
 CellID->1224892054],
Cell[3002, 81, 908, 25, 70, "Usage",
 CellID->982511436],
Cell[CellGroupData[{
Cell[3935, 110, 462, 13, 70, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1938052926],
Cell[CellGroupData[{
Cell[4422, 127, 149, 5, 70, "ExampleSection",
 CellID->1971053233],
Cell[4574, 134, 126, 3, 70, "Input",
 CellID->582053094],
Cell[CellGroupData[{
Cell[4725, 141, 321, 9, 70, "Input",
 CellID->778338773],
Cell[5049, 152, 234, 7, 34, "Output",
 CellID->1742764788]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[5322, 164, 23, 0, 70, "FooterCell"]
}
]
*)

(* End of internal cache information *)

