(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      8937,        288]
NotebookOptionsPosition[      6323,        207]
NotebookOutlinePosition[      8103,        257]
CellTagsIndexPosition[      8019,        252]
WindowTitle->SelectNRandomDifferentFrom - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:GeneticAlgorithms/guide/GeneticAlgorithms"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["GENETICALGORITHMS PACLET SYMBOL", "PacletNameCell"], Cell[TextData[
    Cell[BoxData[
     ActionMenuBox[
      FrameBox["\<\"URL \[RightGuillemet]\"\>",
       StripOnInput->
        False], {"\<\"GeneticAlgorithms/ref/SelectNRandomDifferentFrom\"\>":>
      None, "\<\"Copy Mathematica url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell["GeneticAlgorithms/ref/SelectNRandomDifferentFrom"]}, 
           Visible -> False]]; 
       SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; Null], 
      Delimiter, "\<\"Copy web url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell[
             BoxData[
              MakeBoxes[
               Hyperlink[
               "http://reference.wolfram.com/mathematica/GeneticAlgorithms/\
ref/SelectNRandomDifferentFrom.html"], StandardForm]], "Input", 
             TextClipboardType -> "PlainText"]}, Visible -> False]]; 
       SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; 
       Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
        NotebookLocate[{
          URL[
           StringJoin[
           "http://reference.wolfram.com/mathematica/", 
            "GeneticAlgorithms/ref/SelectNRandomDifferentFrom", ".html"]], 
          None}]}]},
      Appearance->None,
      MenuAppearance->Automatic]],
     LineSpacing->{1.4, 0}]], "AnchorBar"]}
  }]], "AnchorBarGrid",
 GridBoxOptions->{GridBoxItemSize->{"Columns" -> {
     Scaled[0.65], {
      Scaled[0.34]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
   "RowsIndexed" -> {}}},
 CellID->1],

Cell["SelectNRandomDifferentFrom", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"SelectNRandomDifferentFrom", "[", 
       RowBox[{
        StyleBox["n", "TI"], ",", 
        StyleBox["population", "TI"], ",", 
        StyleBox["currentIndex", "TI"]}], "]"}]], "InlineFormula"],
     " \[LineSeparator]SelectNRandomDifferentFrom returns ",
     Cell[BoxData[
      StyleBox["n", "TI"]], "InlineFormula"],
     " indexes from a ",
     Cell[BoxData[
      StyleBox["population", "TI"]], "InlineFormula"],
     " without the possibility of returning the ",
     Cell[BoxData[
      StyleBox["currentIndex", "TI"]], "InlineFormula"],
     "."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->40306782],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->978736269],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\>\"", "]"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->1118330483],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SelectNRandomDifferentFrom", "[", 
  RowBox[{"3", ",", 
   RowBox[{"{", 
    RowBox[{
    "1", ",", "2", ",", "3", ",", "4", ",", "5", ",", "6", ",", "7", ",", "8",
      ",", "9", ",", "10"}], "}"}], ",", "2"}], "]"}]], "Input",
 CellLabel->"In[2]:=",
 CellID->1003549237],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "2"}], "}"}]}], "}"}]], "Output",
 ImageSize->{157, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[2]=",
 CellID->1141959301]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"population", "=", 
  RowBox[{"RandomInteger", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"1", ",", "100"}], "}"}], ",", "10"}], 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"SelectNRandomDifferentFrom", "[", 
  RowBox[{"9", ",", "population", ",", "10"}], "]"}]}], "Input",
 CellLabel->"In[3]:=",
 CellID->1739520733],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "27", ",", "9", ",", "67", ",", "6", ",", "39", ",", "47", ",", "70", ",", 
   "59", ",", "5", ",", "7"}], "}"}]], "Output",
 ImageSize->{229, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[3]=",
 CellID->1295053349],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "47", ",", "27", ",", "39", ",", "6", ",", "70", ",", "9", ",", "59", ",", 
   "5", ",", "67"}], "}"}]], "Output",
 ImageSize->{211, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[3]=",
 CellID->1214874298]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"SelectNRandomDifferentFrom - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2013, 12, 18, 7, 22, 43.262558}", "context" -> 
    "GeneticAlgorithms`", "keywords" -> {}, "index" -> True, "label" -> 
    "GeneticAlgorithms Paclet Symbol", "language" -> "en", "paclet" -> 
    "GeneticAlgorithms", "status" -> "None", "summary" -> 
    "SelectNRandomDifferentFrom[n, population, currentIndex] \
SelectNRandomDifferentFrom returns n indexes from a population without the \
possibility of returning the currentIndex.", "synonyms" -> {}, "title" -> 
    "SelectNRandomDifferentFrom", "type" -> "Symbol", "uri" -> 
    "GeneticAlgorithms/ref/SelectNRandomDifferentFrom"}, "LinkTrails" -> "", 
  "SearchTextTranslated" -> ""},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> 
  "9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, 2013)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[3829, 109, 460, 13, 70, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->40306782]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 7877, 245}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[619, 21, 76, 1, 70, "SymbolColorBar"],
Cell[698, 24, 200, 5, 70, "LinkTrail"],
Cell[901, 31, 2007, 45, 70, "AnchorBarGrid",
 CellID->1],
Cell[2911, 78, 69, 1, 70, "ObjectName",
 CellID->1224892054],
Cell[2983, 81, 821, 24, 70, "Usage",
 CellID->982511436],
Cell[CellGroupData[{
Cell[3829, 109, 460, 13, 70, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->40306782],
Cell[CellGroupData[{
Cell[4314, 126, 148, 5, 70, "ExampleSection",
 CellID->978736269],
Cell[4465, 133, 127, 3, 70, "Input",
 CellID->1118330483],
Cell[CellGroupData[{
Cell[4617, 140, 297, 8, 70, "Input",
 CellID->1003549237],
Cell[4917, 150, 362, 13, 34, "Output",
 CellID->1141959301]
}, Open  ]],
Cell[CellGroupData[{
Cell[5316, 168, 349, 10, 70, "Input",
 CellID->1739520733],
Cell[5668, 180, 298, 9, 34, "Output",
 CellID->1295053349],
Cell[5969, 191, 288, 9, 34, "Output",
 CellID->1214874298]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[6296, 205, 23, 0, 70, "FooterCell"]
}
]
*)

(* End of internal cache information *)

