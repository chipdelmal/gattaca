(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     11644,        365]
NotebookOptionsPosition[      8553,        272]
NotebookOutlinePosition[     10546,        325]
CellTagsIndexPosition[     10461,        320]
WindowTitle->CrossoverPopulationEdgeRecombination - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 StyleBox[ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:guide/Mathematica"],
  FontSlant->"Italic"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["GENETICALGORITHMS PACLET SYMBOL", "PacletNameCell"], Cell[TextData[
    Cell[BoxData[
     ActionMenuBox[
      FrameBox["\<\"URL \[RightGuillemet]\"\>",
       StripOnInput->
        False], {"\<\"GeneticAlgorithms/ref/CrossoverEdgesPopulation\"\>":>
      None, "\<\"Copy Mathematica url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell["GeneticAlgorithms/ref/CrossoverEdgesPopulation"]}, Visible -> 
           False]]; SelectionMove[
        DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; Null], 
      Delimiter, "\<\"Copy web url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell[
             BoxData[
              MakeBoxes[
               Hyperlink[
               "http://reference.wolfram.com/mathematica/GeneticAlgorithms/\
ref/CrossoverEdgesPopulation.html"], StandardForm]], "Input", 
             TextClipboardType -> "PlainText"]}, Visible -> False]]; 
       SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; 
       Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
        NotebookLocate[{
          URL[
           StringJoin[
           "http://reference.wolfram.com/mathematica/", 
            "GeneticAlgorithms/ref/CrossoverEdgesPopulation", ".html"]], 
          None}]}]},
      Appearance->None,
      MenuAppearance->Automatic]],
     LineSpacing->{1.4, 0}]], "AnchorBar"]}
  }]], "AnchorBarGrid",
 GridBoxOptions->{GridBoxItemSize->{"Columns" -> {
     Scaled[0.65], {
      Scaled[0.34]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
   "RowsIndexed" -> {}}},
 CellID->1],

Cell["CrossoverPopulationEdgeRecombination", "ObjectName",
 CellID->1224892054],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{"CrossoverPopulationEdgeRecombination", "[", 
       RowBox[{
        StyleBox["population", "TI"], ",", 
        StyleBox["selectedIndexes", "TI"], ",", 
        StyleBox["probability", "TI"]}], "]"}]], "InlineFormula"],
     " \[LineSeparator]Crossover",
     "Population",
     "EdgeRecombination applies an edges crossover operation to a certain ",
     Cell[BoxData[
      StyleBox["population", "TI"]], "InlineFormula"],
     " taking a list of ",
     Cell[BoxData[
      StyleBox["selectedIndexes", "TI"]], "InlineFormula"],
     " as the mating pool and applying the crossover with a certain ",
     Cell[BoxData[
      StyleBox["probability", "TI"]], "InlineFormula"],
     " (otherwise the parents are simply copied to the next generation \
pool)."
    }]]}
  }]], "Usage",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {{None}}, 
   "RowsIndexed" -> {}}},
 CellID->982511436],

Cell[CellGroupData[{

Cell[TextData[Cell[BoxData[
 ButtonBox[
  FrameBox[
   StyleBox[
    RowBox[{"MORE", " ", "INFORMATION"}], "NotesFrameText"],
   StripOnInput->False],
  Appearance->{Automatic, None},
  BaseStyle->None,
  ButtonFunction:>(FrontEndExecute[{
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], All, ButtonCell], 
     FrontEndToken["OpenCloseGroup"], 
     FrontEnd`SelectionMove[
      FrontEnd`SelectedNotebook[], After, CellContents]}]& ),
  Evaluator->None,
  Method->"Preemptive"]]]], "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->408948409],

Cell["\<\
If an odd number of elements is selected the last individual of the pool is \
paired with the first one.\
\>", "Notes",
 CellID->1067943069]
}, Closed]],

Cell[CellGroupData[{

Cell[TextData[ButtonBox["EXAMPLES",
 BaseStyle->None,
 Appearance->{Automatic, None},
 Evaluator->None,
 Method->"Preemptive",
 ButtonFunction:>(FrontEndExecute[{
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], All, ButtonCell], 
    FrontEndToken["OpenCloseGroup"], 
    FrontEnd`SelectionMove[
     FrontEnd`SelectedNotebook[], After, 
     CellContents]}]& )]], "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1248239663],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(1)", "ExampleCount"]
}], "ExampleSection",
 CellID->396268498],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<GeneticAlgorithms`\.1d\>\"", "]"}]], "Input",
 CellLabel->"In[1]:=",
 CellID->471887219],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"palaven", "=", 
  RowBox[{"GenerateRandomPermutationPopulation", "[", 
   RowBox[{"5", ",", "5"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"CrossoverPopulationEdgeRecombination", "[", 
  RowBox[{"palaven", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3"}], "}"}], ",", "1"}], "]"}]}], "Input",
 CellLabel->"In[2]:=",
 CellID->895848923],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "4", ",", "2", ",", "5", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"4", ",", "3", ",", "5", ",", "1", ",", "2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "3", ",", "2", ",", "4", ",", "1"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "1", ",", "2", ",", "3", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"4", ",", "5", ",", "1", ",", "3", ",", "2"}], "}"}]}], 
  "}"}]], "Output",
 ImageSize->{527, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[2]=",
 CellID->417638383],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "4", ",", "2", ",", "5", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "4", ",", "2", ",", "5", ",", "3"}], "}"}]}], 
  "}"}]], "Output",
 ImageSize->{215, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[2]=",
 CellID->1271744685]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"palaven", "=", 
  RowBox[{"GenerateRandomPermutationPopulation", "[", 
   RowBox[{"5", ",", "5"}], "]"}]}], "\[IndentingNewLine]", 
 RowBox[{"CrossoverPopulationEdgeRecombination", "[", 
  RowBox[{"palaven", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "3", ",", "4"}], "}"}], ",", "1"}], "]"}]}], "Input",
 CellLabel->"In[3]:=",
 CellID->1007563819],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"2", ",", "5", ",", "1", ",", "4", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"4", ",", "3", ",", "1", ",", "5", ",", "2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "4", ",", "1", ",", "3", ",", "2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "4", ",", "3", ",", "1", ",", "2"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "4", ",", "3", ",", "1", ",", "2"}], "}"}]}], 
  "}"}]], "Output",
 ImageSize->{527, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[3]=",
 CellID->308182204],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"2", ",", "3", ",", "1", ",", "5", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"2", ",", "3", ",", "1", ",", "5", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "4", ",", "3", ",", "2", ",", "1"}], "}"}]}], 
  "}"}]], "Output",
 ImageSize->{319, 13},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[3]=",
 CellID->2035229711]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"CrossoverPopulationEdgeRecombination - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2013, 12, 18, 7, 22, 22.957478}", "context" -> 
    "GeneticAlgorithms`", "keywords" -> {}, "index" -> True, "label" -> 
    "GeneticAlgorithms Paclet Symbol", "language" -> "en", "paclet" -> 
    "GeneticAlgorithms", "status" -> "None", "summary" -> 
    "CrossoverPopulationEdgeRecombination[population, selectedIndexes, \\ \
probability] CrossoverPopulationEdgeRecombination applies an edges crossover \
operation to a certain population taking a list of selectedIndexes as the \
mating pool and applying the crossover with a certain probability (otherwise \
the parents are simply copied to the next generation pool).", 
    "synonyms" -> {}, "title" -> "CrossoverPopulationEdgeRecombination", 
    "type" -> "Symbol", "uri" -> 
    "GeneticAlgorithms/ref/CrossoverEdgesPopulation"}, "LinkTrails" -> "", 
  "SearchTextTranslated" -> ""},
CellContext->"Global`",
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"]}, Visible -> False, 
  FrontEndVersion -> 
  "9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, 2013)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{
 "PrimaryExamplesSection"->{
  Cell[4821, 141, 462, 13, 70, "PrimaryExamplesSection",
   CellTags->"PrimaryExamplesSection",
   CellID->1248239663]}
 }
*)
(*CellTagsIndex
CellTagsIndex->{
 {"PrimaryExamplesSection", 10317, 313}
 }
*)
(*NotebookFileOutline
Notebook[{
Cell[629, 21, 76, 1, 70, "SymbolColorBar"],
Cell[708, 24, 209, 6, 70, "LinkTrail"],
Cell[920, 32, 2000, 45, 70, "AnchorBarGrid",
 CellID->1],
Cell[2923, 79, 79, 1, 70, "ObjectName",
 CellID->1224892054],
Cell[3005, 82, 1021, 27, 70, "Usage",
 CellID->982511436],
Cell[CellGroupData[{
Cell[4051, 113, 580, 17, 70, "NotesSection",
 CellGroupingRules->{"SectionGrouping", 50},
 CellID->408948409],
Cell[4634, 132, 150, 4, 70, "Notes",
 CellID->1067943069]
}, Closed]],
Cell[CellGroupData[{
Cell[4821, 141, 462, 13, 70, "PrimaryExamplesSection",
 CellTags->"PrimaryExamplesSection",
 CellID->1248239663],
Cell[CellGroupData[{
Cell[5308, 158, 148, 5, 70, "ExampleSection",
 CellID->396268498],
Cell[5459, 165, 130, 3, 70, "Input",
 CellID->471887219],
Cell[CellGroupData[{
Cell[5614, 172, 364, 9, 70, "Input",
 CellID->895848923],
Cell[5981, 183, 632, 18, 34, "Output",
 CellID->417638383],
Cell[6616, 203, 366, 12, 34, "Output",
 CellID->1271744685]
}, Open  ]],
Cell[CellGroupData[{
Cell[7019, 220, 375, 9, 70, "Input",
 CellID->1007563819],
Cell[7397, 231, 632, 18, 34, "Output",
 CellID->308182204],
Cell[8032, 251, 455, 14, 34, "Output",
 CellID->2035229711]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[8526, 270, 23, 0, 70, "FooterCell"]
}
]
*)

(* End of internal cache information *)

