(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     22097,        631]
NotebookOptionsPosition[     18445,        527]
NotebookOutlinePosition[     20896,        585]
CellTagsIndexPosition[     20853,        582]
WindowTitle->MAX-SAT Problem - Wolfram Mathematica
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[" ", "TutorialColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 0}}],

Cell[TextData[{
 ButtonBox["Mathematica",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:GeneticAlgorithms/guide/GeneticAlgorithms"],
 StyleBox[" > ", "LinkTrailSeparator"]
}], "LinkTrail"],

Cell[BoxData[GridBox[{
   {Cell["GENETIC ALGORITHMS TUTORIAL", "PacletNameCell"], Cell[TextData[Cell[
    BoxData[
     ActionMenuBox[
      FrameBox["\<\"URL \[RightGuillemet]\"\>",
       StripOnInput->
        False], {"\<\"GeneticAlgorithms/tutorial/MAX-SAT Problem\"\>":>
      None, "\<\"Copy Mathematica url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell["GeneticAlgorithms/tutorial/MAX-SAT Problem"]}, Visible -> 
           False]]; SelectionMove[
        DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; Null], 
      Delimiter, "\<\"Copy web url\"\>":>
      Module[{DocumentationSearch`Private`nb$}, 
       DocumentationSearch`Private`nb$ = NotebookPut[
          Notebook[{
            Cell[
             BoxData[
              MakeBoxes[
               Hyperlink[
               "http://reference.wolfram.com/mathematica/GeneticAlgorithms/\
tutorial/MAX-SAT Problem.html"], StandardForm]], "Input", TextClipboardType -> 
             "PlainText"]}, Visible -> False]]; 
       SelectionMove[DocumentationSearch`Private`nb$, All, Notebook]; 
       FrontEndTokenExecute[DocumentationSearch`Private`nb$, "Copy"]; 
       NotebookClose[DocumentationSearch`Private`nb$]; 
       Null], "\<\"Go to web url\"\>":>FrontEndExecute[{
        NotebookLocate[{
          URL[
           StringJoin[
           "http://reference.wolfram.com/mathematica/", 
            "GeneticAlgorithms/tutorial/MAX-SAT Problem", ".html"]], None}]}]},
      Appearance->None,
      MenuAppearance->Automatic]],
     LineSpacing->{1.4, 0}]], "AnchorBar"]}
  }]], "AnchorBarGrid",
 GridBoxOptions->{GridBoxItemSize->{"Columns" -> {
     Scaled[0.65], {
      Scaled[0.34]}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
   "RowsIndexed" -> {}}},
 CellID->1],

Cell[CellGroupData[{

Cell["MAX-SAT Problem", "Title",
 CellID->509267359],

Cell["\<\
The maximum satisfiability problem is the following one: given a logic \
formula in disjunctive normal form find the combination of inputs that yields \
the maximum number of true clauses.\
\>", "Text",
 CellID->1534169418],

Cell["XXXX.", "Caption",
 CellID->1891092685],

Cell[CellGroupData[{

Cell[TextData[{
 "The first thing we need to do to solve our problem is to protect the value \
of the variable \"x\" that we need to be \"free\" so that we can create and \
evaluate our logic formulas. After that we define a function that generates \
random DNF and we also need a pair of functions that allow us to convert our \
populations from binary (0,1) to logical (False, True); something that is \
needed as the package works only with binary values although ",
 StyleBox["Mathematica",
  FontSlant->"Italic"],
 " evaluates a logic function with logic values and two helper functions that \
will help us evaluate our fitness function."
}], "MathCaption",
 CellID->836781195],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Protect", "[", "x", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"GenerateRandomDNFList", "[", 
   RowBox[{
   "clauseMaxSize_", ",", "numberOfVariables_", ",", "numberOfClauses_", ",", 
    "x_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
     "listOfVariables", ",", "clauseSize", ",", "index", ",", "negative", ",",
       "dnfList"}], "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"dnfList", "=", "\[IndentingNewLine]", 
      RowBox[{"Table", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{
         RowBox[{"clauseSize", "=", 
          RowBox[{"RandomInteger", "[", 
           RowBox[{"{", 
            RowBox[{"1", ",", "clauseMaxSize"}], "}"}], "]"}]}], ";", 
         "\[IndentingNewLine]", 
         RowBox[{"Table", "[", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{
            RowBox[{"index", "=", 
             RowBox[{"RandomInteger", "[", 
              RowBox[{"{", 
               RowBox[{"1", ",", "numberOfVariables"}], "}"}], "]"}]}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"negative", "=", 
             RowBox[{"RandomInteger", "[", 
              RowBox[{"{", 
               RowBox[{"0", ",", "1"}], "}"}], "]"}]}], ";", 
            "\[IndentingNewLine]", 
            RowBox[{"If", "[", 
             RowBox[{
              RowBox[{"negative", "\[Equal]", "0"}], ",", 
              RowBox[{"x", "[", "index", "]"}], ",", 
              RowBox[{"!", 
               RowBox[{"x", "[", "index", "]"}]}]}], "]"}]}], 
           "\[IndentingNewLine]", ",", 
           RowBox[{"{", 
            RowBox[{"i", ",", "1", ",", "clauseSize"}], "}"}]}], "]"}]}], 
        "\[IndentingNewLine]", ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", "numberOfClauses"}], "}"}]}], "]"}]}], 
     ";", "\[IndentingNewLine]", "dnfList"}]}], "\[IndentingNewLine]", 
   "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ConvertPopulationToLogic", "[", "population_", "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Table", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Table", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{
           RowBox[{
            RowBox[{"population", "[", 
             RowBox[{"[", "i", "]"}], "]"}], "[", 
            RowBox[{"[", "j", "]"}], "]"}], "\[Equal]", "0"}], ",", "False", 
          ",", "True"}], "]"}], "\[IndentingNewLine]", ",", 
        RowBox[{"{", 
         RowBox[{"j", ",", "1", ",", 
          RowBox[{"Length", "[", 
           RowBox[{"population", "[", 
            RowBox[{"[", "i", "]"}], "]"}], "]"}]}], "}"}]}], "]"}], 
      "\[IndentingNewLine]", ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", "population", "]"}]}], "}"}]}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"ConvertPopulationToBinary", "[", "population_", "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Table", "[", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Table", "[", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{
           RowBox[{
            RowBox[{"population", "[", 
             RowBox[{"[", "i", "]"}], "]"}], "[", 
            RowBox[{"[", "j", "]"}], "]"}], "\[Equal]", "False"}], ",", "0", 
          ",", "1"}], "]"}], "\[IndentingNewLine]", ",", 
        RowBox[{"{", 
         RowBox[{"j", ",", "1", ",", 
          RowBox[{"Length", "[", 
           RowBox[{"population", "[", 
            RowBox[{"[", "i", "]"}], "]"}], "]"}]}], "}"}]}], "]"}], 
      "\[IndentingNewLine]", ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", "population", "]"}]}], "}"}]}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"CountTrues", "[", "boolList_", "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "counter", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Total", "[", 
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"If", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"boolList", "[", 
           RowBox[{"[", "i", "]"}], "]"}], "\[Equal]", "True"}], ",", "1", 
         ",", "0"}], "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "1", ",", 
         RowBox[{"Length", "[", "boolList", "]"}]}], "}"}]}], "]"}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EvaluateDisjunctionClause", "[", 
   RowBox[{"list_", ",", "chromosome_", ",", "x_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "assignations", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"assignations", "=", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"x", "[", "i", "]"}], "\[Rule]", 
         RowBox[{"chromosome", "[", 
          RowBox[{"[", "i", "]"}], "]"}]}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", 
          RowBox[{"Length", "[", "chromosome", "]"}]}], "}"}]}], "]"}]}], ";",
      "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Apply", "[", 
       RowBox[{"Or", ",", "list"}], "]"}], "/.", "assignations"}]}]}], 
   "\[IndentingNewLine]", "]"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"EvaluateConjunctionsList", "[", 
   RowBox[{"list_", ",", "chromosome_", ",", "x_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"trueClauses", ",", "boolList"}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"trueClauses", "=", "0"}], ";", "\[IndentingNewLine]", 
     RowBox[{"boolList", "=", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"EvaluateDisjunctionClause", "[", 
         RowBox[{
          RowBox[{"list", "[", 
           RowBox[{"[", "i", "]"}], "]"}], ",", "chromosome", ",", "x"}], 
         "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "1", ",", 
          RowBox[{"Length", "[", "list", "]"}]}], "}"}]}], "]"}]}]}]}], 
   "\[IndentingNewLine]", "]"}]}]}], "Input",
 CellLabel->"In[1]:=",
 CellID->2058623809]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Now we are ready to generate the function we want to work on:\
\>", "MathCaption",
 CellID->677454377],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"numberOfVariables", "=", "6"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"functionToAnalyze", "=", 
  RowBox[{"GenerateRandomDNFList", "[", 
   RowBox[{"3", ",", "numberOfVariables", ",", "10", ",", "x"}], 
   "]"}]}]}], "Input",
 CellLabel->"In[2]:=",
 CellID->1679203177],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "1", "]"}]}], ",", 
     RowBox[{"!", 
      RowBox[{"x", "[", "2", "]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "2", "]"}]}], ",", 
     RowBox[{"!", 
      RowBox[{"x", "[", "3", "]"}]}], ",", 
     RowBox[{"x", "[", "3", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "2", "]"}]}], ",", 
     RowBox[{"x", "[", "5", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"!", 
     RowBox[{"x", "[", "4", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"x", "[", "2", "]"}], ",", 
     RowBox[{"x", "[", "1", "]"}], ",", 
     RowBox[{"x", "[", "4", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "[", "4", "]"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "5", "]"}]}], ",", 
     RowBox[{"!", 
      RowBox[{"x", "[", "5", "]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"!", 
     RowBox[{"x", "[", "3", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"!", 
     RowBox[{"x", "[", "5", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"!", 
      RowBox[{"x", "[", "1", "]"}]}], ",", 
     RowBox[{"!", 
      RowBox[{"x", "[", "4", "]"}]}]}], "}"}]}], "}"}]], "Output",
 ImageSize->{542, 27},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[2]=",
 CellID->2110079741]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Once we have defined our objective we need our fitness function so that we \
can assign a fitness value to our algorithm. This fitness function counts the \
amount of True clauses in our function:\
\>", "MathCaption",
 CellID->1844993314],

Cell[BoxData[
 RowBox[{
  RowBox[{"EvaluateLogicPopulation", "[", 
   RowBox[{"function_", ",", "population_", ",", "x_"}], "]"}], ":=", 
  RowBox[{"Module", "[", 
   RowBox[{
    RowBox[{"{", "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"CountTrues", "[", 
       RowBox[{"EvaluateConjunctionsList", "[", 
        RowBox[{"function", ",", 
         RowBox[{"population", "[", 
          RowBox[{"[", "i", "]"}], "]"}], ",", "x"}], "]"}], "]"}], 
      "\[IndentingNewLine]", ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "1", ",", 
        RowBox[{"Length", "[", "population", "]"}]}], "}"}]}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 CellLabel->"In[3]:=",
 CellID->1422686853]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
This time we will be using a uniform crossover operator with a binary flip \
mutation and a tournament parents selection scheme. So we define the \
parameters for the genetic algorithm:\
\>", "MathCaption",
 CellID->1855386990],

Cell[BoxData[{
 RowBox[{
  RowBox[{"populationSize", "=", "30"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"parentsRoundSize", "=", "3"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"crossoverProbability", "=", ".65"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"crossoverUniformProbability", "=", ".1"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"mutationProbability", "=", ".002"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"generations", "=", "100"}], ";"}]}], "Input",
 CellLabel->"In[4]:=",
 CellID->1748496010],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"population", "=", 
    RowBox[{"GenerateRandomBinaryPopulation", "[", 
     RowBox[{"populationSize", ",", "numberOfVariables"}], "]"}]}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"dataArray", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", "0", "}"}], ",", 
     RowBox[{"{", "0", "}"}], ",", 
     RowBox[{"{", "0", "}"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"populationTemp", "=", "population"}], ";"}], 
  "\[IndentingNewLine]", 
  RowBox[{"(*", "CycleStarts", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"output", "=", "\[IndentingNewLine]", 
   RowBox[{"Table", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{
      RowBox[{"populationLogic", "=", 
       RowBox[{"ConvertPopulationToLogic", "[", "population", "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"populationScores", "=", 
       RowBox[{"EvaluateLogicPopulation", "[", 
        RowBox[{"functionToAnalyze", ",", "populationLogic", ",", "x"}], 
        "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", "Reaping", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"reaping", "=", 
       RowBox[{"TournamentParentsSelection", "[", 
        RowBox[{
        "populationScores", ",", "parentsRoundSize", ",", "populationSize"}], 
        "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", "Crossover", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"crossoverPopulation", "=", 
       RowBox[{"CrossoverPopulationUniform", "[", 
        RowBox[{
        "populationTemp", ",", "reaping", ",", "crossoverProbability", ",", 
         "crossoverUniformProbability"}], "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"(*", "Mutation", "*)"}], "\[IndentingNewLine]", 
      RowBox[{"mutatedPopulation", "=", 
       RowBox[{"MutateBinaryPopulationFlip", "[", 
        RowBox[{"populationTemp", ",", "mutationProbability"}], "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"(*", 
       RowBox[{
       "Data", " ", "Manipulation", " ", "and", " ", "Generational", " ", 
        "Replacement"}], "*)"}], "\[IndentingNewLine]", 
      RowBox[{"populationTemp", "=", "mutatedPopulation"}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"dataArray", "=", 
       RowBox[{"AppendGenerationFitnessData", "[", 
        RowBox[{"dataArray", ",", "populationScores"}], "]"}]}], ";"}], 
     "\[IndentingNewLine]", ",", 
     RowBox[{"{", 
      RowBox[{"i", ",", "1", ",", "generations"}], "}"}]}], "]"}]}], 
  ";"}]}], "Input",
 CellLabel->"In[5]:=",
 CellID->806214522],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PlotSingleRunGA", "[", "dataArray", "]"}]], "Input",
 CellLabel->"In[6]:=",
 CellID->675480961],

Cell[BoxData[
 GraphicsBox[{
   {RGBColor[0, 0, 1], LineBox[CompressedData["
1:eJxdyzlKREEYhdGLkaGBgYGBioiISDvP9rOdZ23nVOjYLdTSakkuQaUFeaeg
+Dl83OmPz/5gJMng5//e4fvqpv2a4Zn5u6N4DI/jCTyJp7B3Fs/hebyAF/ES
7uBlvIJX8Rpexxt4E2/hbbyDd/Ee3scHuIubtgvOIR2nR8c5ouMc03FO6Din
dJwzOs45HeeCjnNJx7mi41zTcW7oOLd0nDs6zn3bDS644jywxwVXnD57XHDF
eWSPC644T+xxwRXnmT0uuOK8sMcFV5xX9rjgivPGHhdccd7Z//sbSahyzA==

     "]]}, 
   {RGBColor[0, 1, 0], LineBox[CompressedData["
1:eJxdyzlKhEEUhdGLkaGBgYGCioiIiPNs9+88T+2cCh27hVpabUTX4BK06UD6
PCiKw8ed+vjsdIeSdP9e7+/fTzuD13x/9W6i6XMYj+BRPIbH8SSexjN4Fs/h
ebyAF/ESXsYreBWv4XW8gTfxFt7GO3gX7+F93MJt3Ay64BzQcQ7pOEd0nGM6
zgkd55SOc0bHOafjXNBxLuk4V3ScazrODR3nlo5zR8e5H3SDC644D+xxwRWn
wx4XXHEe2eOCK84Te1xwxXlmjwuuOC/sccEV55U9LrjivLHHBVecd/b//gVp
u3dS
     "]]}, 
   {RGBColor[1, 0, 0], LineBox[CompressedData["
1:eJxdyzlKREEYhdGLkaGBgYGBioiISDvP9rOdZ23nVOjYLdTSakkuQaUFeaeg
+Dl83OmPz/5gJMng5//e4fvqpv2a4Zn4u6N4DI9j7ySewjN4Fs/hebyAF/ES
7uBlvIJX8Rpexxt4E2/hbbyDd/Ee3scHuIubtgvOIR2nR8c5ouMc03FO6Din
dJwzOs45HeeCjnNJx7mi41zTcW7oOLd0nDs6zn3bDS644jywxwVXnD57XHDF
eWSPC644T+xxwRXnmT0uuOK8sMcFV5xX9rjgivPGHhdccd7Z//sbK2Ru5A==

     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->True,
  AxesOrigin->{0, Automatic},
  PlotRangeClipping->True]], "Output",
 ImageSize->{184, 117},
 ImageMargins->{{0, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 1}},
 CellLabel->"Out[6]=",
 CellID->362244034]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[" ", "FooterCell"]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"MAX-SAT Problem - Wolfram Mathematica",
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "built" -> "{2013, 12, 18, 7, 22, 46.333103}", "context" -> 
    "GeneticAlgorithms`", "keywords" -> {}, "index" -> True, "label" -> 
    "Genetic Algorithms Tutorial", "language" -> "en", "paclet" -> 
    "GeneticAlgorithms", "status" -> "None", "summary" -> 
    "The maximum satisfiability problem is the following one: given a logic \
formula in disjunctive normal form find the combination of inputs that yields \
the maximum number of true clauses. XXXX. The first thing we need to do to \
solve our problem is to protect the value of the variable \"x\" that we need \
to be \"free\" so that we can create and evaluate our logic formulas. After \
that we define a function that generates random DNF and we also need a pair \
of functions that allow us to convert our populations from binary (0,1) to \
logical (False, True); something that is needed as the package works only \
with binary values although Mathematica evaluates a logic function with logic \
values and two helper functions that will help us evaluate our fitness \
function.", "synonyms" -> {}, "title" -> "MAX-SAT Problem", "type" -> 
    "Tutorial", "uri" -> "GeneticAlgorithms/tutorial/MAX-SAT Problem"}, 
  "LinkTrails" -> "", "SearchTextTranslated" -> ""},
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, \
2013)",
StyleDefinitions->Notebook[{
   Cell[
    StyleData[
    StyleDefinitions -> FrontEnd`FileName[{"Wolfram"}, "Reference.nb"]]], 
   Cell[
    StyleData["Input"], CellContext -> "Global`"], 
   Cell[
    StyleData["Output"], CellContext -> "Global`"], 
   Cell[
    StyleData["TutorialMoreAboutSection"], 
    CellGroupingRules -> {"SectionGrouping", 30}], 
   Cell[
    StyleData["RelatedTutorialsSection"], 
    CellGroupingRules -> {"SectionGrouping", 30}], 
   Cell[
    StyleData["TutorialRelatedLinksSection"], 
    CellGroupingRules -> {"SectionGrouping", 30}]}, Visible -> False, 
  FrontEndVersion -> 
  "9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (January 25, 2013)", 
  StyleDefinitions -> "Default.nb"]
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[608, 21, 78, 1, 70, "TutorialColorBar"],
Cell[689, 24, 200, 5, 70, "LinkTrail"],
Cell[892, 31, 1969, 44, 70, "AnchorBarGrid",
 CellID->1],
Cell[CellGroupData[{
Cell[2886, 79, 52, 1, 70, "Title",
 CellID->509267359],
Cell[2941, 82, 233, 5, 70, "Text",
 CellID->1534169418],
Cell[3177, 89, 45, 1, 70, "Caption",
 CellID->1891092685],
Cell[CellGroupData[{
Cell[3247, 94, 682, 12, 70, "MathCaption",
 CellID->836781195],
Cell[3932, 108, 6385, 168, 70, "Input",
 CellID->2058623809]
}, Open  ]],
Cell[CellGroupData[{
Cell[10354, 281, 112, 3, 70, "MathCaption",
 CellID->677454377],
Cell[CellGroupData[{
Cell[10491, 288, 308, 8, 70, "Input",
 CellID->1679203177],
Cell[10802, 298, 1510, 53, 48, "Output",
 CellID->2110079741]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[12361, 357, 248, 5, 70, "MathCaption",
 CellID->1844993314],
Cell[12612, 364, 745, 20, 70, "Input",
 CellID->1422686853]
}, Open  ]],
Cell[CellGroupData[{
Cell[13394, 389, 237, 5, 70, "MathCaption",
 CellID->1855386990],
Cell[13634, 396, 572, 17, 70, "Input",
 CellID->1748496010],
Cell[14209, 415, 2611, 62, 70, "Input",
 CellID->806214522],
Cell[CellGroupData[{
Cell[16845, 481, 119, 3, 70, "Input",
 CellID->675480961],
Cell[16967, 486, 1412, 34, 138, "Output",
 CellID->362244034]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[18418, 525, 23, 0, 70, "FooterCell"]
}
]
*)

(* End of internal cache information *)

