(* Paclet Info File *)

(* created 2012/10/07*)

Paclet[
    Name -> "GeneticAlgorithms",
    Version -> "0.0.1",
    MathematicaVersion -> "6+",
    Description -> "The Genetic Algorithms package provides with the necessary tools to create evolutionary computing applications.",
    Creator -> "Hector Manuel Sanchez Castellanos",
    Extensions -> 
        {
            {"Documentation", Resources -> 
                {"Guides/Main"}
            , Language -> "English"}
        }
]


